# LCE Engine
A basic game to learn opengl, I might evolve this into something larger than just a learning experiment
---
Dependencies

- [cglm](https://github.com/recp/cglm)

- ``sudo apt install libx11-dev libepoxy-dev libglfw3`` 

---
Building
```
make
make run
```
The executable will be in bin/ as lce-beta

---
![](https://i.gyazo.com/85a0eef469922fc9d16b96312c4fdb88.png)