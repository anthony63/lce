NAME=bin/lce-beta

CC=gcc
FLAGS=-Wall -Wextra -pedantic
DEBUG_FLAGS=-g -fsanitize=address
SRC=src/*.c src/**/*.c
LIBS=-lglfw3 -lX11 -ldl -lpthread -lm -lepoxy -lcglm
OBJ=
RM =rm -rf

all: $(NAME)

$(NAME): $(SRC)
	mkdir -p bin
	$(CC) $(FLAGS) $(SRC) -o $(NAME) $(LIBS)

fclean:
	$(RM) $(NAME)

re: fclean all

run: re
	./$(NAME)

debug: fclean
	$(CC) $(FLAGS) $(DEBUG_FLAGS) $(SRC) -o $(NAME) $(LIBS)
