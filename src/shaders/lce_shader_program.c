#include "lce_shader_program.h"

#include <stdio.h>
#include <stdlib.h>

#include <epoxy/gl.h>

static int load_shader(const char* file, int type) {
    FILE* shader_file = fopen(file, "r");
    GLchar* buffer = 0;
    size_t length;

    if(shader_file == NULL) {
        printf("Failed to open shader file: %s\n", file);
        exit(-1);
    }

    fseek(shader_file, 0, SEEK_END);
    length = ftell(shader_file);
    fseek(shader_file, 0, SEEK_SET);
    buffer = malloc(length + 1);
    if(buffer) {
        fread(buffer, 1, length, shader_file);
    }

    fclose(shader_file);
    buffer[length] = '\0';
    GLuint shader_id = glCreateShader(type);
    glShaderSource(shader_id, 1, (const GLchar* const*)&buffer, NULL);
    glCompileShader(shader_id);

    GLint status;
    glGetShaderiv(shader_id, GL_COMPILE_STATUS, &status);
    if(status == GL_FALSE) {
        GLint info_log_length;
        glGetShaderiv(shader_id, GL_INFO_LOG_LENGTH, &info_log_length);

        GLchar* info_log = malloc(sizeof(GLchar) * (info_log_length + 1));
        glGetShaderInfoLog(shader_id, info_log_length, NULL, info_log);

        printf("[LCE] FAILED TO COMPILE SHADER FILE \"%s\"\n[LCE] SHADER COMPILE LOG:\n%s\n", file, info_log);
        free(buffer);
        free(info_log);
        exit(-1);
    }else printf("[LCE] COMPILED SHADER \"%s\"\n", file);

    free(buffer);
    return shader_id;
}

lce_shader_program_t* lce_shader_program_init(const char* vs_path, const char* fs_path, void (*bind_attribs)(int program_id), void (*get_all_uniform_locations)(int program_id)) {
    int vs_id = load_shader(vs_path, GL_VERTEX_SHADER);
    int fs_id = load_shader(fs_path, GL_FRAGMENT_SHADER);
    int program_id = glCreateProgram();
    glAttachShader(program_id, vs_id);
    glAttachShader(program_id, fs_id);
    bind_attribs(program_id);
    glLinkProgram(program_id);
    glValidateProgram(program_id);
    printf("[LCE] SHADER PROGRAM(%d) VALIDATED!\n", program_id);
    lce_shader_program_t* sp = malloc(sizeof *sp);
    sp->program_id = program_id;
    sp->vs_id = vs_id;
    sp->fs_id = fs_id;
    get_all_uniform_locations(program_id);
    return sp;
}


void lce_shader_program_start(lce_shader_program_t* sp){
    glUseProgram(sp->program_id);
}

void lce_shader_program_stop(__attribute__((unused)) lce_shader_program_t* sp){
    glUseProgram(0);
}

void lce_shader_program_cleanup(lce_shader_program_t* sp) {
    lce_shader_program_stop(sp);
    glDetachShader(sp->program_id, sp->vs_id);
    glDetachShader(sp->program_id, sp->fs_id);
    glDeleteShader(sp->vs_id);
    glDeleteShader(sp->fs_id);
    glDeleteProgram(sp->program_id);
    free(sp);
}

int lce_shader_program_get_uniform_location(int program_id, char* uniform_name) {
    int uniform_loc = glGetUniformLocation(program_id, uniform_name);
    printf("[LCE] Got uniform \"%s\" location: %d\n", uniform_name, uniform_loc);
    return uniform_loc;
}

void lce_shader_program_bind_attrib(int program_id, int attrib, const char* name){
    glBindAttribLocation(program_id, attrib, name);
}

void lce_shader_program_load_float(int location, float value) {
    printf("[LCE] Load float (%d) = %f\n", location, value);
    glUniform1f(location, value);
}

void lce_shader_program_load_vector(int location, vec3 vector) {
    printf("[LCE] Load float (%d) = {%f,%f,%f}\n", location, vector[0], vector[1], vector[2]);
    glUniform3f(location, vector[0], vector[1], vector[2]);
}

void lce_shader_program_load_matrix(int location, mat4 matrix) {
    // printf("[LCE] Load matrix (%d) = {\n\t{%f, %f, %f, %f},\n\t{%f, %f, %f, %f},\n\t{%f, %f, %f, %f},\n\t{%f, %f, %f, %f}\n}\n", location,
    //     matrix[0][0], matrix[0][1], matrix[0][2], matrix[0][3],
    //     matrix[1][0], matrix[1][1], matrix[1][2], matrix[1][3],
    //     matrix[2][0], matrix[2][1], matrix[2][2], matrix[2][3],
    //     matrix[3][0], matrix[3][1], matrix[3][2], matrix[3][3]);
    glUniformMatrix4fv(location, 1, GL_FALSE, (float*)matrix);
}