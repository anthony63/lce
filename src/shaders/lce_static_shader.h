#pragma once

#include "lce_shader_program.h"
#include "../util/lce_util.h"
#include "../entities/lce_camera.h"
#include <cglm/cglm.h>

lce_shader_program_t* lce_static_shader_init(void);
void lce_static_shader_load_transform_matrix(mat4 matrix);
void lce_static_shader_load_projection_matrix(mat4 matrix);
void lce_static_shader_load_view_matrix(lce_camera_t* camera);