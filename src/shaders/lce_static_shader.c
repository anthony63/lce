#include "lce_static_shader.h"

const char* VERTEX_FILE_PATH = "shaders/shader.vs";
const char* FRAGMENT_FILE_PATH = "shaders/shader.fs";

static int transform_matrix_location = 0;
static int projection_matrix_location = 0;
static int view_matrix_location = 0;

static void bind_attribs(int program_id) {
    lce_shader_program_bind_attrib(program_id, 0, "position");
    lce_shader_program_bind_attrib(program_id, 1, "texture_coords");
}

static void get_all_uniform_locations(int program_id) {
    transform_matrix_location = lce_shader_program_get_uniform_location(program_id, "transform_matrix");
    projection_matrix_location = lce_shader_program_get_uniform_location(program_id, "projection_matrix");
    view_matrix_location = lce_shader_program_get_uniform_location(program_id, "view_matrix");
}

void lce_static_shader_load_transform_matrix(mat4 matrix) {
    lce_shader_program_load_matrix(transform_matrix_location, matrix);
}

void lce_static_shader_load_projection_matrix(mat4 matrix) {
    lce_shader_program_load_matrix(projection_matrix_location, matrix);
}

void lce_static_shader_load_view_matrix(lce_camera_t* camera) {
    mat4 mat;
    lce_create_view_matrix(mat, camera);
    lce_shader_program_load_matrix(view_matrix_location, mat);
}

lce_shader_program_t* lce_static_shader_init(void) {
    lce_shader_program_t* sp = lce_shader_program_init(VERTEX_FILE_PATH, FRAGMENT_FILE_PATH, &bind_attribs, &get_all_uniform_locations);
    return sp;
}