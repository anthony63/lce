#pragma once

#include <cglm/cglm.h>

typedef struct {
    int program_id;
    int vs_id;
    int fs_id;
} lce_shader_program_t;

lce_shader_program_t* lce_shader_program_init(const char* vs_path, const char* fs_path, void (*bind_attribs)(int program_id), void (*get_all_uniform_locations)(int program_id));
void lce_shader_program_start(lce_shader_program_t* sp);
void lce_shader_program_stop(lce_shader_program_t* sp);
void lce_shader_program_cleanup(lce_shader_program_t* sp);
void lce_shader_program_bind_attrib(int program_id, int attrib, const char* name);
int lce_shader_program_get_uniform_location(int program_id, char* uniform_name);
void lce_shader_program_bind_attrib(int program_id, int attrib, const char* name);
void lce_shader_program_load_float(int location, float value);
void lce_shader_program_load_vector(int location, vec3 vector);
void lce_shader_program_load_matrix(int location, mat4 matrix);