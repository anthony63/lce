#include <stdio.h>
#include <cglm/cglm.h>

#include "engine/lce_window.h"

#define TITLE "LCE Engine | v0.0.1"
#define WIDTH 1280
#define HEIGHT 720

int main() {
    struct lce_window* win = lce_window_init(TITLE, WIDTH, HEIGHT);
    lce_window_run(win);
    lce_window_destroy(win);
}
