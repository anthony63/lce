#pragma once

#include <cglm/cglm.h>

#include <stdlib.h>

typedef struct {
    vec3 position;
    float pitch,yaw,roll;
    float speed,sens;
} lce_camera_t;

lce_camera_t* lce_camera_init();
void lce_camera_move(lce_camera_t* camera, void* win);
void lce_camera_destroy(lce_camera_t* camera);