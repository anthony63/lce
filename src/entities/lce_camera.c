#include "lce_camera.h"
#include <GLFW/glfw3.h>

lce_camera_t* lce_camera_init() {
    lce_camera_t* cam = malloc(sizeof *cam);
    *cam = (lce_camera_t) {0};

    // hardcoding these in for now, smh...
    cam->speed = 0.02;
    cam->sens = 1.0;

    return cam;
}

void lce_camera_destroy(lce_camera_t* camera) {
    free(camera);
}

void lce_camera_move(lce_camera_t* camera, void* win) {
    camera->position[2] += camera->speed * (GET_KEY(GLFW_KEY_S) - GET_KEY(GLFW_KEY_W));
    camera->position[0] += camera->speed * (GET_KEY(GLFW_KEY_D) - GET_KEY(GLFW_KEY_A));
    camera->position[1] += camera->speed * (GET_KEY(GLFW_KEY_SPACE) - GET_KEY(GLFW_KEY_LEFT_SHIFT));
    camera->pitch       += camera->sens  * (GET_KEY(GLFW_KEY_K) - GET_KEY(GLFW_KEY_I));
    camera->yaw         += camera->sens  * (GET_KEY(GLFW_KEY_L) - GET_KEY(GLFW_KEY_J));
}