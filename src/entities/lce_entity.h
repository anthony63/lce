#pragma once

#include "../models/lce_textured_model.h"
#include <string.h>
#include <cglm/cglm.h>

typedef struct {
    lce_textured_model_t* model;
    float position[3];
    float rx, ry, rz;
    float scale;
} lce_entity_t;

lce_entity_t* lce_entity_init(lce_textured_model_t* model, float* position, float rx, float ry, float rz, float scale);
void lce_entity_destroy(lce_entity_t* entity);
void lce_entity_add_position(lce_entity_t* entity, float dx, float dy, float dz);
void lce_entity_add_rotation(lce_entity_t* entity, float rx, float ry, float rz);