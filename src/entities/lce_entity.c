#include "lce_entity.h"

lce_entity_t* lce_entity_init(lce_textured_model_t* model, float position[3], float rx, float ry, float rz, float scale) {
    lce_entity_t* entity = malloc(sizeof *entity);
    entity->model = model;
    entity->position[0] = position[0];
    entity->position[1] = position[1];
    entity->position[2] = position[2];
    entity->rx = rx;
    entity->ry = ry;
    entity->rz = rz;
    entity->scale = scale;
    return entity;
}

void lce_entity_destroy(lce_entity_t* entity) {
    free(entity->model);
    free(entity);
}

void lce_entity_add_position(lce_entity_t* entity, float dx, float dy, float dz){
    entity->position[0] += dx;
    entity->position[1] += dy;
    entity->position[2] += dz;
}

void lce_entity_add_rotation(lce_entity_t* entity, float rx, float ry, float rz){
    entity->rx += rx;
    entity->ry += ry;
    entity->rz += rz;
}