#pragma once

#include "../models/lce_raw_model.h"
#include "../shaders/lce_static_shader.h"
#include "../entities/lce_entity.h"
#include "../util/lce_util.h"

void lce_renderer_prepare(int width, int height, lce_shader_program_t* sp);
void lce_renderer_render(lce_entity_t* entity);