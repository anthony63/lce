#include "lce_loader.h"
#include "../util/lce_id_list.h"

#define STB_IMAGE_IMPLEMENTATION
#include "../../vendor/std_image.h"

#include <epoxy/gl.h>

#define DATA_LEN(x) (sizeof(x)/sizeof(x[0]))

static lce_id_list_t* vao_ids = NULL;
static lce_id_list_t* vbo_ids = NULL;
static lce_id_list_t* tex_ids = NULL;

void lce_loader_init(int max_vaos, int max_vbos, int max_texures) {
    vao_ids = lce_id_list_init(max_vaos);
    vbo_ids = lce_id_list_init(max_vbos);
    tex_ids = lce_id_list_init(max_texures);
}

static int create_vao(void) {
    GLuint vao_id;
    glGenVertexArrays(1, &vao_id);
    printf("[LCE] VAO(%d) Created\n", vao_id);
    lce_id_list_push(vao_ids, vao_id);
    glBindVertexArray(vao_id);
    return vao_id;
}

static void create_vbo(int attrib, int coord_size, float data[], int data_length) {
    GLuint vbo_id;
    glGenBuffers(1, &vbo_id);
    printf("[LCE] VBO(%d) Created\n", vbo_id);
    lce_id_list_push(vbo_ids, vbo_id);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_id);
    glBufferData(GL_ARRAY_BUFFER, data_length * sizeof(float), data, GL_STATIC_DRAW);
    glVertexAttribPointer(attrib, coord_size, GL_FLOAT, GL_FALSE, 0, (void*)0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void unbind_vao() {
    glBindVertexArray(0);
}

static void bind_indices_buffer(int indices[], int indices_length) {
    GLuint ebo_id;
    glGenBuffers(1, &ebo_id);
    printf("[LCE] VBO(%d) Created\n", ebo_id);
    lce_id_list_push(vbo_ids, ebo_id);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo_id);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices_length * sizeof(int), indices, GL_STATIC_DRAW);
}

lce_raw_model_t* __lce_load_to_vao(float positions[], int positions_count, int indices[], int indices_count, float texture_coords[], int texture_coords_count) {
    int vao_id = create_vao();
    bind_indices_buffer(indices, indices_count);
    create_vbo(0, 3, positions, positions_count);
    create_vbo(1, 2, texture_coords, texture_coords_count);
    unbind_vao();
    lce_raw_model_t* model = lce_raw_model_init(vao_id, indices_count);
    return model;
}

int lce_loader_load_texture(const char* file) {
    int width, height, num_channels;
    unsigned char* data = stbi_load(file, &width, &height, &num_channels, 0);
    printf("[LCE] Texture Image %s: %dx%d | channels: %d\n", file, width, height, num_channels);
    GLuint texture_id;
    glGenTextures(1, &texture_id);
    glBindTexture(GL_TEXTURE_2D, texture_id);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);	
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    lce_id_list_push(tex_ids, texture_id);
    if(data) {
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
        glGenerateMipmap(GL_TEXTURE_2D);
    }else {
        printf("Failed to load texture: %s\n", file);
        exit(-1);
    }
    stbi_image_free(data);
    return texture_id;
}

void lce_loader_cleanup(void) {
    for(int i = 0; i < lce_id_list_size(vao_ids); i++) {
        GLuint vao_id = lce_id_list_pop(vao_ids);
        printf("[LCE] VAO(%d) Deleted\n", vao_id);
        glDeleteVertexArrays(1, &vao_id);
    }
    for(int i = 0; i < lce_id_list_size(vbo_ids); i++) {
        GLuint vbo_id = lce_id_list_pop(vbo_ids);
        printf("[LCE] VBO(%d) Deleted\n", vbo_id);
        glDeleteVertexArrays(1, &vbo_id);
    }
    for(int i = 0; i < lce_id_list_size(tex_ids); i++) {
        GLuint tex_id = lce_id_list_pop(tex_ids);
        printf("[LCE] TEX(%d) Deleted\n", tex_id);
        glDeleteTextures(1, &tex_id);
    }
}