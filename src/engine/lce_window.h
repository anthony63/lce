#pragma once

#include <GLFW/glfw3.h>

#include "lce_loader.h"
#include "lce_renderer.h"
#include "../entities/lce_entity.h"
#include "../entities/lce_camera.h"
#include "../shaders/lce_static_shader.h"

struct lce_window {
    GLFWwindow* glfw_win;
    lce_entity_t* entity;
    lce_shader_program_t* static_shader;
    lce_camera_t* camera;
    int current_key;
};

struct lce_window* lce_window_init(const char* title, int width, int height);
void lce_window_run(struct lce_window* lce_win);
void lce_window_destroy(struct lce_window* lce_win);