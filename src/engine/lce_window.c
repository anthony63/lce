#include "lce_window.h"

#include "../geometry/cube.h"

#include <stdio.h>
#include <stdlib.h>

struct lce_window* lce_window_init(const char* title, int width, int height) {
    struct lce_window* lce_win = malloc(sizeof *lce_win);

    if(glfwInit() != GLFW_TRUE) {
        printf("Failed to initialize glfw!\n");
        exit(-1);
    }

    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);

    glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);

    lce_win->glfw_win = glfwCreateWindow(width, height, title, NULL, NULL);
    glfwMakeContextCurrent(lce_win->glfw_win);
    lce_loader_init(4096, 4096, 1024);

    lce_raw_model_t* raw_model = lce_loader_load_to_vao(lce_cube_vertices, lce_cube_indices, lce_cube_texture_coords);
    lce_textured_model_t* model = lce_textured_model_init(raw_model, lce_loader_load_texture("assets/gawr.png"));
    lce_win->entity = lce_entity_init(model, (float[3]){0.0, 0.0, -1.0}, 0, 0, 0, 1);
    lce_win->static_shader = lce_static_shader_init();
    lce_win->camera = lce_camera_init();
    return lce_win;
}

static void render(struct lce_window* lce_win) {
    lce_shader_program_start(lce_win->static_shader);
    lce_static_shader_load_view_matrix(lce_win->camera);
    lce_renderer_render(lce_win->entity);
    lce_shader_program_stop(lce_win->static_shader);
    glfwSwapBuffers(lce_win->glfw_win);
}

static void update(struct lce_window* lce_win) {
    lce_entity_add_rotation(lce_win->entity, 1.0, 1.0, 0.0);
    lce_camera_move(lce_win->camera, lce_win->glfw_win);
}

void lce_window_run(struct lce_window* lce_win) {
    int width, height;
    glfwGetWindowSize(lce_win->glfw_win, &width, &height);
    lce_renderer_prepare(width, height, lce_win->static_shader);
    while(!glfwWindowShouldClose(lce_win->glfw_win)) {
        glfwPollEvents();
        update(lce_win);
        render(lce_win);
    }
}

void lce_window_destroy(struct lce_window* lce_win) {
    lce_camera_destroy(lce_win->camera);
    lce_shader_program_cleanup(lce_win->static_shader);
    lce_loader_cleanup();
    lce_entity_destroy(lce_win->entity);
    free(lce_win);
}
