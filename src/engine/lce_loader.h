#pragma once

#include "../models/lce_raw_model.h"

#define lce_loader_load_to_vao(POSITIONS, INDICES, TEXTURE_COORDS) \
    __lce_load_to_vao(POSITIONS, sizeof(POSITIONS)/sizeof(POSITIONS[0]), INDICES, sizeof(INDICES)/sizeof(INDICES[0]), TEXTURE_COORDS, sizeof(TEXTURE_COORDS) / sizeof(TEXTURE_COORDS[0]))

void lce_loader_init(int max_vaos, int max_vbos, int max_textures);
lce_raw_model_t* __lce_load_to_vao(float positions[], int positions_count, int indices[], int indices_count, float texture_coords[], int texture_coords_count);
int lce_loader_load_texture(const char* file);
void lce_loader_cleanup(void);