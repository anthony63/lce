#include "lce_renderer.h"
#include <epoxy/gl.h>

void lce_renderer_prepare(int width, int height, lce_shader_program_t* sp) {
    glEnable(GL_DEPTH_TEST);
    glClearColor(0.325, 0.271, 0.51, 1.0);
    mat4 projection_matrix;
    glm_perspective(70.0, (float)width/(float)height, 0.1, 1000.0, projection_matrix); 
    lce_shader_program_start(sp);
    lce_static_shader_load_projection_matrix(projection_matrix);
    lce_shader_program_stop(sp);
}

void lce_renderer_render(lce_entity_t* entity) {
    lce_textured_model_t* model = entity->model;
    lce_raw_model_t* raw_model = model->raw_model;
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glBindVertexArray(raw_model->vao_id);
    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);
    mat4 trans_matrix;
    lce_create_transform_matrix(trans_matrix, entity->position, entity->rx, entity->ry, entity->rz, entity->scale);
    lce_static_shader_load_transform_matrix(trans_matrix);
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, model->textured_id);
    glDrawElements(GL_TRIANGLES, raw_model->vertex_count, GL_UNSIGNED_INT, 0);
    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);
    glBindVertexArray(0);
}