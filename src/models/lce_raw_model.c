#include "lce_raw_model.h"

#include <stdlib.h>

lce_raw_model_t* lce_raw_model_init(int vao_id, int vertex_count) {
    lce_raw_model_t* raw_model = malloc(sizeof *raw_model);
    raw_model->vao_id = vao_id;
    raw_model->vertex_count = vertex_count;
    return raw_model;
}

void lce_raw_model_destroy(lce_raw_model_t* raw_model) {
    free(raw_model);
}