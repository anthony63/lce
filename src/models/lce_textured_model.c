#include "lce_textured_model.h"

#include <stdlib.h>

lce_textured_model_t* lce_textured_model_init(lce_raw_model_t* raw_model, int texture_id) {
    lce_textured_model_t* model = malloc(sizeof *model);
    model->raw_model = raw_model;
    model->textured_id = texture_id;
    return model;
}

void lce_textured_model_destroy(lce_textured_model_t* textured_model) {
    free(textured_model->raw_model);
    free(textured_model);
}