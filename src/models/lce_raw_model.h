#pragma once

typedef struct {
    int vao_id;
    int vertex_count;
} lce_raw_model_t;

lce_raw_model_t* lce_raw_model_init(int vao_id, int vertex_count);

void lce_raw_model_destroy(lce_raw_model_t* raw_model);