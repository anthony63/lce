#pragma once

#include "lce_raw_model.h"

typedef struct {
    int textured_id;
    lce_raw_model_t* raw_model;
} lce_textured_model_t;

lce_textured_model_t* lce_textured_model_init(lce_raw_model_t* raw_model, int texture_id);
void lce_textured_model_destroy(lce_textured_model_t* textured_model);