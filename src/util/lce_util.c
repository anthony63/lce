#include "lce_util.h"

void lce_create_transform_matrix(mat4 mat, float* translation, float rx, float ry, float rz, float scale) {
    mat4 m;
    glm_mat4_identity(m);
    glm_translate(m, translation);
    glm_rotate(m, glm_rad(rx), (float[]){1, 0, 0});
    glm_rotate(m, glm_rad(ry), (float[]){0, 1, 0});
    glm_rotate(m, glm_rad(rz), (float[]){0, 0, 1});
    glm_scale(m, (float[]){scale, scale, scale});
    glm_mat4_copy(m, mat);
}

void lce_create_view_matrix(mat4 mat, lce_camera_t* camera) {
    mat4 m;
    glm_mat4_identity(m);
    glm_rotate(m, glm_rad(camera->pitch), (float[]){1, 0, 0});
    glm_rotate(m, glm_rad(camera->yaw), (float[]){0, 1, 0});
    float* cp = camera->position;
    float ncp[3] = {-cp[0], -cp[1], -cp[2]};
    glm_translate(m, ncp);
    glm_mat4_copy(m, mat);
}