#include "lce_id_list.h"

#include <stdio.h>
#include <stdlib.h>

static int is_empty(lce_id_list_t* list) {
    return list->top == -1;
}
static int is_full(lce_id_list_t* list) {
    return list->top == list->max_size - 1;
}

lce_id_list_t* lce_id_list_init(int max_size) {
    lce_id_list_t* list = malloc(sizeof *list);
    list->max_size = max_size;
    list->top = -1;
    list->ids = malloc(sizeof(int) * max_size);
    return list;
}

int lce_id_list_size(lce_id_list_t* list) {
    return list->top + 1;
}

void lce_id_list_push(lce_id_list_t* list, int id) {
    if(is_full(list)) {
        printf("LCE_ID_LIST OVERFLOW!\n");
        exit(-1);
    }
    list->ids[++list->top] = id;
}

int lce_id_list_peek(lce_id_list_t* list) {
    if(is_empty(list)) {
        printf("LCE_ID_LIST UNDERFLOW!\n");
        exit(-1);
    }
    return list->ids[list->top];
}

int lce_id_list_pop(lce_id_list_t* list) {
    if(is_empty(list)) {
        printf("LCE_ID_LIST UNDERFLOW!\n");
        exit(-1);
    }
    int id = list->ids[list->top--];
    return id;
}