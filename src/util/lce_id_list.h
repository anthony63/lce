#pragma once

typedef struct {
    int max_size;
    int top;
    int *ids;
} lce_id_list_t;

lce_id_list_t* lce_id_list_init(int max_size);
int lce_id_list_size(lce_id_list_t* list);
void lce_id_list_push(lce_id_list_t* list, int id);
int lce_id_list_peek(lce_id_list_t* list);
int lce_id_list_pop(lce_id_list_t* list);