#pragma once

#include <cglm/cglm.h>
#include "../entities/lce_camera.h"

void lce_create_transform_matrix(mat4 mat, float* translation, float rx, float ry, float rz, float scale);
void lce_create_view_matrix(mat4 mat, lce_camera_t* camera);